package br.com.devmedia.java.hibernate;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

public class Main {

	private static EntityManagerFactory emf;
	
	public static void main(String[] args) {
		emf = Persistence.createEntityManagerFactory("hibernatejpa");
		
		//insert("Fazer o mercado", "Amanh�, 11h00");
//		list();
//		findByID(1L);
//		findByTitulo("Comprar");
//		update(1L, new Lembrete("Comprar p�o", "Hoje, 20h:10"));
//		delete(2L);
	}

	private static void insert(String titulo, String descricao) {
		Lembrete lembrete = new Lembrete();
		lembrete.setTitulo(titulo);
		lembrete.setDescricao(descricao);
		
		EntityManager em = emf.createEntityManager();
		
		try {
			em.getTransaction().begin();
			em.persist(lembrete);
			em.getTransaction().commit();
		} catch (Exception e) {
			em.getTransaction().rollback();
			System.out.println("INSERT: " + e.getMessage());
		} finally {
			em.close();
			emf.close();
		}
	}

	private static void list() {
		List<Lembrete> lembretes = null;
		EntityManager em = emf.createEntityManager();
		
		try {
			lembretes = em.createQuery("from Lembrete").getResultList();
		} catch (Exception e) {
			System.out.println("LIST ALL: " + e.getMessage());
		} finally {
			em.close();
			emf.close();
		}
		
		if (lembretes != null) {
			for (Lembrete l : lembretes) {
				System.out.println(l.toString());
			}
		}
	}
	
	private static void findByID(Long id) {
		EntityManager em = emf.createEntityManager();
		
		Lembrete lembrete = null;
		
		try {
			lembrete = em.find(Lembrete.class, id);
			System.out.println(lembrete);
		} catch (Exception e) {
			System.out.println(e.getMessage());
		} finally {
			em.close();
			emf.close();
		}
	}
	
	private static void findByTitulo(String titulo) {
		List<Lembrete> lembretes = null;
		
		EntityManager em = emf.createEntityManager();
		
		try {
			lembretes = em.createQuery("from Lembrete l where l.titulo LIKE" +
				" '%" + titulo + "%'").getResultList();
		} catch (Exception e) {
			System.out.println("LIST ALL: " + e.getMessage());
		} finally {
			em.close();
			emf.close();
		}
		
		if (lembretes != null) {
			for (Lembrete lembrete : lembretes) {
				System.out.println(lembrete.toString());
			}
		}
	}
	
	private static void update(Long id, Lembrete lembrete) {
		EntityManager em = emf.createEntityManager();
		
		try {
			Lembrete l = em.find(Lembrete.class, id);
			l.setTitulo(lembrete.getTitulo());
			l.setDescricao(lembrete.getDescricao());
			
			em.getTransaction().begin();
			em.merge(l);
			em.getTransaction().commit();
		} catch (Exception e) {
			em.getTransaction().rollback();
			
			System.out.println("UPDATE: " + e.getMessage());
		} finally {
			em.close();
			emf.close();
		}
	}

	private static void delete(Long id) {
		EntityManager em = emf.createEntityManager();
		
		try {
			Lembrete lembrete = em.find(Lembrete.class, id);
			
			em.getTransaction().begin();
			em.remove(lembrete);
			em.getTransaction().commit();
			
		} catch (Exception e) {
			em.getTransaction().rollback();
			System.out.println("DELETE: " + e.getMessage());
		} finally {
			em.close();
			emf.close();
		}
	}
	
}
